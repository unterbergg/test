$(document).ready(function(){
	$('.filter').on("change", function(){
		$.ajax({
			url:$(this).data('action'),
			data:{action:"filter",tax: $(this).val()},
			type:"POST", 
			success:function(data){
				$(".posts_example").html(data);
			}
		});
		return false;
	});
});

