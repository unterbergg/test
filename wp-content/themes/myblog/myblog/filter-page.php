<?php
/*
 Template name: Filter
*/

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<div>
			<?php 
			$termss = $wpdb->get_results("SELECT term_id, name  FROM wp_terms as t left join wp_term_relationships as tr on tr.term_taxonomy_id = t.term_id left join wp_posts as p on tr.object_id = p.ID left join wp_postmeta as pm on p.ID = pm.post_id where pm.meta_key = 'corporate' and pm.meta_value = 1 GROUP BY term_id");

				if( $termss ) :?> <!-- $terms = get_terms( array('taxonomy' => 'custom_tax', 'orderby' => 'name', 'hide_empty' => true) ) -->
					<select name="categoryfilter" class="filter" data-action="<?= site_url();?>/wp-admin/admin-ajax.php">
						<option value="0">Select category...</option>
					<?foreach ($termss as $term ) :?>
						<option value="<?=$term->term_id;?>"><?= $term->name?></option>; 
					<?endforeach;?>
					</select>
				<?endif;
			?>
		<div class="posts_example">
			<?php 
				$args = array(
					'post_type' => 'very_custom',
					'numberposts' => 20,
					'orderby'     => 'date',
					'order'       => 'DESC'
				);

				$posts = get_posts($args);
				
				foreach ($posts as $key => $post) {
					echo "<pre>";
					print_r($post->post_title);
					echo "</pre>";
				}
				
			 ?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
