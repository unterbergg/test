<?php
/*
 Template name: Main
*/

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<div class="posts_example">
			<?php 
				$hour = date("g");
				echo "curent hour is " . $hour;
				$args = array(
					'post_type' => 'very_custom',
					'numberposts' => 20,
					'orderby'     => 'date',
					'order'       => 'DESC',			
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'relation' => 'AND',
							array(
					            'key' => 'available_from',
					            'value' => (int)$hour,
					            'compare' => '<',
					            'type' => 'NUMERIC',
					        ),
					        array(
					            'key' => 'available_to',
					            'value' => (int)$hour,
					            'compare' => '>',
					            'type' => 'NUMERIC',
					        ),
						),				        
				        array(
				            'key' => 'protected',
				            'value' => 0,
				        ),
				    ),
				);

				$posts = get_posts($args);
				echo "<br>";

				$func = function($post) {
			    	$arr_keys['author'] = get_the_author_meta('display_name',$post->post_author);//$post->post_author;
					$arr_keys['title'] = $post->post_title;
					$arr_keys['protected'] = (bool)get_post_meta( $post->ID, 'protected', true );//get_field("protected",$post->ID);
					$arr_keys['available_from'] = get_post_meta( $post->ID, 'available_from', true );//get_field("available_from",$post->ID);
					$arr_keys['available_to'] = get_post_meta( $post->ID, 'available_to', true );//get_field("available_to",$post->ID);
					$arr_keys['content'] = $post->post_content;
					$arr_keys['date'] = $post->post_date;
					return $arr_keys;
				};

				// echo "<pre>";
				// var_dump(array_map($func, $posts));
				// echo "</pre>";
				$price = get_post_meta(39, "price", true);
				var_dump($price);
			 ?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
